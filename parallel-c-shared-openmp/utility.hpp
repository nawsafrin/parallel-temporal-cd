//#ifndef _UTILITY_HPP
#define	_UTILITY_HPP

#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <iterator>
#include <map>
//#include <string.h>
#include <bits/stdc++.h> 

//#include <iostream> 
#include <string> 

using namespace std;


inline vector<int> Intersection_v(vector<int> a, int sa, vector<int> b, int sb)
{
	int s, i, k;
    vector<int>c;
	for (s=i=k=0; k<sb && i<sa; i++) {
		while (k<sb && b[k]<a[i]) k++;
		if (k<sb && b[k]==a[i])
		{

            c.push_back(a[i]);// c[s++] = a[i];
            s++;


		}
	}
	/*cout<<"Print within function"<<endl;
	for (vector<int>::iterator it = c.begin() ; it != c.end(); ++it)
    {
        cout << ' ' << *it;
    }*/
	return c;
}

inline vector<int> Intersection_v(vector<int> a, vector<int> b)
{
    //std::vector<int>::iterator it, ls;
    vector<int>c;
    set_intersection(a.begin(), a.end(), b.begin(), b.end(),inserter(c, c.begin()));


    return c;
}

inline vector<int> Difference_v(vector<int> a, vector<int> b)
{
    vector<int>c;
    set_difference(a.begin(), a.end(), b.begin(), b.end(),inserter(c, c.begin()));


    return c;
}


inline vector<pair<int,int>> Difference_vp(vector<pair<int,int>> a, vector<pair<int,int>> b)    ///Teseted Working
{
    vector<pair<int,int>>c;
    set_difference(a.begin(), a.end(), b.begin(), b.end(),inserter(c, c.begin()));


    return c;
}

//function to print the queue
inline 
void printQueue(queue<int> q)
{
    //printing content of queue 
    while (!q.empty()){
        cout<<" "<<q.front();
        q.pop();
    }
    cout<<endl;
}

inline void printVector(vector<int> v)
{
    for(int i=0;i<v.size();i++)
    {
        cout << " " << v[i] ;
    }
    cout<<endl;
}


inline bool node_in_list(int node, vector<int> nodelist)
{
    for(int i=0;i<nodelist.size();i++)
    {
        if(nodelist[i]==node)return true;
    }
    return false;
}
/*
inline bool has_edge(int node_u, int node_v, vector<int> nodelist)
{
    for(int i=0;i<nodelist.size();i++)
    {
        if(nodelist[i]==node)return true;
    }
    return false;
}
*/
inline int find_map_key_max(map <int,vector<int>> community_node_list)    //Tested Working
{

    int max=0;
    map<int,vector<int>>:: iterator itr;
    for (itr = community_node_list.begin(); itr != community_node_list.end(); itr++)
    {
        if(itr->first>max)max=itr->first;
    }
    return max;
}


inline vector<int> int_list_from_string(string str)     //tested working
{
    vector<int>c;
    // Used to split string around spaces. 
    istringstream ss(str); 
  
    // Traverse through all words 
    do { 
        // Read a word 
        string word; 
        ss >> word; 
        if(word.compare("\0")!=0)
        {
            // Print the read word 
           // cout << "String:" <<  word << endl; 

            int p=stoi(word);
           // cout << "Int:"    << p << endl;
            c.push_back(p);
        }
        
       
  
        // While there is more to read 
    } while (ss);



    return c;

}


