#include <iostream>
#include <iterator>
#include <vector>
#include <map>
#include <queue>

#include <unistd.h>
#include <sys/time.h>


#include <string.h>
#include <sstream>
#include <algorithm>

#include "utility.hpp"
#include <bits/stdc++.h>

#include <omp.h>
using namespace std;


char *input_file = NULL;
char *output_file = NULL;
int num_snap=2;
int start=1;
int comm_format=1; //1=community-list format; 0=node-comm format;


map< int,int > comm_list;    //contains <node-id,comm-id>
map< int,vector<int> > node_list;    //contains <node-id, neighbor-list>
map< int,vector<int> > community_node_list;  //contains <community-id, node-list>
///////int comm_list[100];


////condition variables

int add_1, add_2, add_3, add_4, add_5;
int del_1, del_2, del_3, del_4, del_5;





long long current_timestamp() {
    struct timeval te;
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    // printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}



vector<int> nodes_in_comm(int comm)
{
    //    iterating over all value of umap
    map<int,int>:: iterator itr;
    vector<int>result;
    for (itr = comm_list.begin(); itr != comm_list.end(); itr++)
    {
        if(itr->second==comm)result.push_back(itr->first);

        // itr works as a pointer to pair<string, double>
        // type itr->first stores the key part  and
        // itr->second stroes the value part
        //cout << itr->first << "  " << itr->second << endl;
    }
    return result;

}

void add_node(int u)
{
    pair<int,vector<int>>temp1 (u,{});
    node_list.insert(temp1);

}

void add_comm(int u)
{
    //int p=community_node_list.size();
    int p=find_map_key_max(community_node_list);
    pair<int,vector<int>>temp1 ((p+1),{u});
    community_node_list.insert(temp1);
    pair<int,int> temp2 (u,(p+1));
    comm_list.insert(temp2);
}


int count_distinct_comm_list()
{
    map<int,int>:: iterator itr2;
    vector<int> temp;
    /*int max=0;
    for (itr2 = comm_list.begin(); itr2 != comm_list.end(); itr2++)
    {
        int temp;
        temp=itr2->second;
        if(temp>max)max=temp;
    }*/
    for (itr2 = comm_list.begin(); itr2 != comm_list.end(); itr2++)
    {
        if(community_node_list.count(itr2->second)!=0)
        {
            temp.push_back(itr2->second);
        }

        /*if(community_node_list.count(itr2->second)==0)
        {
            pair<int,vector<int>>temp1 ((itr2->second),{itr2->first});
            community_node_list.insert(temp1);
        }
        else
        {
            temp=community_node_list[itr2->second];
            temp.push_back(itr2->first);
            community_node_list[itr2->second]=temp;

        }*/


    }
    return temp.size();
}

void adjust_community_node_list()
{
    map<int,int>:: iterator itr2;
    vector<int> temp_distinct_comm;
    /*int max=0;
    for (itr2 = comm_list.begin(); itr2 != comm_list.end(); itr2++)
    {
        int temp;
        temp=itr2->second;
        if(temp>max)max=temp;
    }*/
    for (itr2 = comm_list.begin(); itr2 != comm_list.end(); itr2++)
    {
        vector<int> temp;
        if(community_node_list.count(itr2->second)==0)
        {
            temp_distinct_comm.push_back(itr2->second);
            pair<int,vector<int>>temp1 ((itr2->second),{itr2->first});
            community_node_list.insert(temp1);
        }
        else
        {
            temp=community_node_list[itr2->second];
            temp.push_back(itr2->first);
            community_node_list[itr2->second]=temp;

        }


    }

    cout << "Distinct Community: " << temp_distinct_comm.size() << endl;
}
void adjust_comm_list()
{

    map<int,vector<int>>:: iterator itr2;
    for (itr2 = community_node_list.begin(); itr2 != community_node_list.end(); itr2++)
    {
        vector<int> temp;
        temp=itr2->second;
        for  (int i=0;i<temp.size();i++)
        {
            //int p=comm_list.find(temp[i])->first;
            comm_list[temp[i]]=itr2->first;
        }

    }


}

inline int degree(int node)
{

    vector<int>neigh_node;
    neigh_node=node_list.find(node)->second;

    return neigh_node.size();

}

void print_community_list()
{
    map<int,vector<int>>:: iterator itr;
    cout << "community_node_list" << endl;
    for (itr = community_node_list.begin(); itr != community_node_list.end(); itr++)
    {
        cout  << itr->first << " : "<< endl;
        vector<int>temp=itr->second;
        for(int i=0;i<temp.size();i++)
        {
            cout << temp[i] << " ";
        }
        cout << endl;
    }
    cout << "comm_list" << endl;
    map<int,int>:: iterator itr2;
    for (itr2 = comm_list.begin(); itr2 != comm_list.end(); itr2++)
    {
        cout  << itr2->first << "&& " << itr2->second << endl;

    }

}

void print_community_list_temp(map<int,vector<int>> community_node_list_temp, map<int,int> comm_list_temp )
{
    map<int,vector<int>>:: iterator itr;
    cout << "community_node_list" << endl;
    for (itr = community_node_list_temp.begin(); itr != community_node_list_temp.end(); itr++)
    {
        cout  << itr->first << endl;
        vector<int>temp=itr->second;
        for(int i=0;i<temp.size();i++)
        {
            cout << temp[i] << " ";
        }
        cout << endl;
    }
    cout << "comm_list" << endl;
    map<int,int>:: iterator itr2;
    for (itr2 = comm_list_temp.begin(); itr2 != comm_list_temp.end(); itr2++)
    {
        cout  << itr2->first << "&& " << itr2->second << endl;

    }

}

void add_edge(int u, int v)
{

    if(node_list.count(u)==0 && node_list.count(v)==0)
    {
        pair<int,vector<int>>temp1 (u,{v});
        node_list.insert(temp1);

        pair<int,vector<int>>temp2 (v,{u});
        node_list.insert(temp2);
    }
    else if(node_list.count(u)==0 && node_list.count(v)==1)
    {
        node_list.find(v)->second.push_back(u);

        pair<int,vector<int>>temp1 (u,{v});
        node_list.insert(temp1);
    }
    else if(node_list.count(u)==1 && node_list.count(v)==0)
    {
        node_list.find(u)->second.push_back(v);

        pair<int,vector<int>>temp2 (v,{u});
        node_list.insert(temp2);
    }
    else
    {
        node_list.find(u)->second.push_back(v);
        node_list.find(v)->second.push_back(u);
    }


}

void delete_edge(int u, int v)
{
    if(degree(u)==1 && degree(v)==1)
    {
        node_list.erase(u);
        node_list.erase(v);
    }
    else if(degree(u)==1 && degree(v)!=1)
    {
        node_list.erase(u);
        vector<int>neigh_node, temp_neigh;
        neigh_node=node_list.find(v)->second;

        for(int i=0;i<neigh_node.size();i++)
        {
            if(neigh_node[i]!=u)temp_neigh.push_back(neigh_node[i]);
        }
        node_list[v]=temp_neigh;

    }
    else if(degree(v)==1 && degree(u)!=1)
    {
        node_list.erase(v);
        vector<int>neigh_node, temp_neigh;
        neigh_node=node_list.find(u)->second;

        for(int i=0;i<neigh_node.size();i++)
        {
            if(neigh_node[i]!=v)temp_neigh.push_back(neigh_node[i]);
        }
        node_list[u]=temp_neigh;

    }
    else
    {
        vector<int>neigh_node, temp_neigh;
        neigh_node=node_list.find(u)->second;

        for(int i=0;i<neigh_node.size();i++)
        {
            if(neigh_node[i]!=v)temp_neigh.push_back(neigh_node[i]);
        }
        node_list[u]=temp_neigh;

        neigh_node.clear();
        temp_neigh.clear();

        neigh_node=node_list.find(v)->second;

        for(int i=0;i<neigh_node.size();i++)
        {
            if(neigh_node[i]!=u)temp_neigh.push_back(neigh_node[i]);
        }
        node_list[v]=temp_neigh;
    }




}

inline bool has_edge(int node1, int node2)
{

    vector<int>neigh_node;
    neigh_node=node_list.find(node1)->second;
    // #pragma omp parallel for
    for(int i=0;i<neigh_node.size();i++)
    {
        if(neigh_node[i]==node2)return true;
    }
    return false;

}



float permanence(int node)
{
    float perm;
    int e_max=0;
    int numerator=0;
    float denominator=0;
    int comm;
    comm=comm_list.find(node)->second;
    vector<int> comm_members;
    //comm_members=nodes_in_comm(comm);
    comm_members=community_node_list.find(comm)->second;

    vector<int>neigh_node;
    neigh_node=node_list.find(node)->second;
    int d_u=neigh_node.size();
    vector<int>internal_neighbors;
    //intersect
    sort(comm_members.begin(),comm_members.end());
    sort(neigh_node.begin(),neigh_node.end());
    internal_neighbors=Intersection_v(comm_members,neigh_node);
    int num_internal_neigh=internal_neighbors.size();

    // cout << endl << "Community Members: " << endl;
    // for (int i=0;i<comm_members.size();i++)
    // {
    //     cout << comm_members[i] << " ";
    // }

    // cout << endl<< "All Neighbors: " << endl;
    // for (int i=0;i<neigh_node.size();i++)
    // {
    //     cout << neigh_node[i] << " ";
    // }

    // cout<< endl << "Internal Neighbors: " << endl;
    // for (int i=0;i<internal_neighbors.size();i++)
    // {
    //     cout << internal_neighbors[i] << " ";
    // }


    map<int,vector<int>>:: iterator itr;
    for (itr = community_node_list.begin(); itr != community_node_list.end(); itr++)
    {
        if(itr->first!=comm)
        {
            vector<int> temp_node, temp_int_res;
            temp_node=itr->second;
            sort(temp_node.begin(),temp_node.end());
            temp_int_res=Intersection_v(temp_node,neigh_node);
            int e_c=temp_int_res.size();
            if(e_c>e_max)e_max=e_c;
        }

    }

    if (e_max==0 && d_u!=0)
    {
        perm=num_internal_neigh/d_u;
    }
    else if(e_max==0 && d_u==0)
    {
        perm=0;
    }
    else
    {
	int comm_size=comm_members.size();
        #pragma omp parallel for collapse(2) shared(comm_members,node,internal_neighbors,numerator)
        for(int i=0;i<comm_size;i++)
        {
            for(int j=0;j<comm_size;j++)
            {
                //cout << "comm_members[" << i << "]=" << comm_members[i] << " comm_members[" << j << "]=" << comm_members[j] << endl;
                //if node_1!=u and node_2!=u and (node_1 in i_neigh) and (node_2 in i_neigh) and (G.has_edge(node_1,node_2) or G.has_edge(node_2,node_1)):
                if((comm_members[i]!=node) && (comm_members[j]!=node) && (comm_members[i]!=comm_members[j])&& (node_in_list(comm_members[i],internal_neighbors)) && (node_in_list(comm_members[j],internal_neighbors)) && (has_edge(comm_members[i],comm_members[j]) || has_edge(comm_members[j],comm_members[i])))
                {
                    #pragma omp critical
                    {numerator++;}
                }
            }
        }
        //cout << "numerator: " << numerator <<endl;
        numerator/=2;
        denominator= num_internal_neigh*(num_internal_neigh-1)/2;
        if (denominator==0)denominator=1;
        float c_in=numerator/denominator;
        perm=float((num_internal_neigh/float(d_u))*(1/float(e_max)))-1.0+float(c_in);

    }
    //cout << "perm(" << node << ")=("<< num_internal_neigh << "/" << d_u << "*" << e_max <<")-1+"<< numerator << "/" << denominator << "="<< perm<< endl;
    return perm;

    /*   map<int,vector<int>>:: iterator itr;
       for (itr = node_list.begin(); itr != node_list.end(); itr++)
       {
           if(itr->first==comm)result.push_back(itr->first);
       }*/

}

float permanence(int node, map< int,int > p_comm_list, map< int,vector<int> > p_community_node_list )
{
    float perm;
    int e_max=0;
    float numerator=0;
    float denominator=0;
    int comm;
    comm=p_comm_list.find(node)->second;
    vector<int> comm_members;
    //comm_members=nodes_in_comm(comm);
    comm_members=p_community_node_list.find(comm)->second;
    vector<int>neigh_node;
    neigh_node=node_list.find(node)->second;
    int d_u=neigh_node.size();
    vector<int>internal_neighbors;
    //intersect
    sort(comm_members.begin(),comm_members.end());
    sort(neigh_node.begin(),neigh_node.end());
    internal_neighbors=Intersection_v(comm_members,neigh_node);
    int num_internal_neigh=internal_neighbors.size();

    map<int,vector<int>>:: iterator itr;
    for (itr = p_community_node_list.begin(); itr != p_community_node_list.end(); itr++)
    {
        if(itr->first!=comm)
        {
            vector<int> temp_node, temp_int_res;
            temp_node=itr->second;
            sort(temp_node.begin(),temp_node.end());
            temp_int_res=Intersection_v(temp_node,neigh_node);
            int e_c=temp_int_res.size();
            if(e_c>e_max)e_max=e_c;
        }

    }
    if (e_max==0 && d_u!=0)
    {
        perm=num_internal_neigh/d_u;
    }
    else if(e_max==0 && d_u==0)
    {
        perm=0;
    }
    else
    {
	int comm_size=comm_members.size();
        #pragma omp parallel for collapse(2) shared(comm_members,node,internal_neighbors,numerator)
        for(int i=0;i<comm_size;i++)
        {
            for(int j=0;j<comm_size;j++)
            {
                if((comm_members[i]!=node) && (comm_members[j]!=node) && (comm_members[i]!=comm_members[j])&& (node_in_list(comm_members[i],internal_neighbors)) && (node_in_list(comm_members[j],internal_neighbors)) && (has_edge(comm_members[i],comm_members[j]) || has_edge(comm_members[j],comm_members[i])))
                {
                    #pragma omp critical
                    {numerator++;}
                }
            }
        }
        numerator/=2;
        denominator= num_internal_neigh*(num_internal_neigh-1)/2;
        if (denominator==0)denominator=1;
        float c_in=numerator/denominator;
        perm=float((num_internal_neigh/float(d_u))*(1/float(e_max)))-1.0+float(c_in);

    }

    return perm;

    /*   map<int,vector<int>>:: iterator itr;
       for (itr = node_list.begin(); itr != node_list.end(); itr++)
       {
           if(itr->first==comm)result.push_back(itr->first);
       }*/

}


float permanence_comm(int comm)
{
    float sum=0;
    vector<int>members=community_node_list.find(comm)->second;




    #pragma omp parallel for reduction(+ : sum)
    for(int i=0;i<members.size();i++)
    {
        sum+=permanence(members[i]);
    }
    sum/=members.size();
    //cout << comm << " = " << sum << endl;
    return sum;

}


float permanence_comm(int comm, map< int,int > p_comm_list, map< int,vector<int> > p_community_node_list)
{
    float sum=0;
    vector<int>members=p_community_node_list.find(comm)->second;
    #pragma omp parallel for reduction(+ : sum)
    for(int i=0;i<members.size();i++)
    {
        sum+=permanence(members[i],p_comm_list,p_community_node_list);
    }
    sum/=members.size();
    return sum;

}


void edge_addition(int u, int v)
{
    //cout << "Adding Edge: " << u <<"& "<< v<<endl;
    if(comm_list.count(u)==0 )
    {
        //cout << "Add Condddddddition 1" <<endl;
        add_1++;
        add_node(u);
        add_comm(u);
    }
    if(comm_list.count(v)==0 )
    {
        //cout << "Add Condddddddition 2" <<endl;
        add_2++;
        add_node(v);
        add_comm(v);
    }
    if(comm_list.find(u)->second!=comm_list.find(v)->second)
    {
        //cout << "Add Condddddddition 3" <<endl;
        add_3++;
        //U & ITS NEIGHBORS    MOVEs
        map<int,int> visited;
        int c_u=comm_list.find(u)->second;

        int c_v=comm_list.find(v)->second;

        //cout << "Community u: "<< c_u << "Community v: "<< c_v <<endl;

        float c_u_perm_old=permanence_comm(c_u);
        float c_v_perm_old=permanence_comm(c_v);

        //cout << "Community u permanance: "<< c_u_perm_old << "Community v permanence: "<< c_v_perm_old <<endl;

        //queue<int> myqueue;

        vector<int> myqueue;

        //temp community list for u
        map< int,vector<int> > temp_community_node_list=community_node_list;
        map< int,int > temp_comm_list=comm_list;


        add_edge(u,v);

        //myqueue.push(u);
        myqueue.push_back(u);

        visited[u]=0;
        //while (!myqueue.empty())
        while (myqueue.size()>0)
        {
            int c=temp_comm_list.find(myqueue.front())->second;
            //cout << "queue[0] community=" << c<< endl;
            vector<int> evaluated;

            map< int,vector<int> > temp_new_community_node_list;
            map< int,int > temp_new_comm_list;

            map<int,int>:: iterator itr;
            ////for vis in visited.keys():
            // #pragma omp parallel for 
            for (itr = visited.begin(); itr!=visited.end(); itr++)
            {
                //if visited[vis]==0 and vis!=v:
                // #pragma omp task
                {
                    if(itr->second==0 && itr->first!=v)
                    {
                        float p_1=permanence(itr->first, temp_comm_list, temp_community_node_list);

                        //temp new community list (temp_comm_list_new=temp_comm_list[:])
                        temp_new_community_node_list=temp_community_node_list;
                        temp_new_comm_list=temp_comm_list;


                        //temp_comm_list_new[c]=list(set(temp_comm_list_new[c])-set([vis]))
                        vector<int> temp_new_comm_mem=temp_new_community_node_list[c];
                        vector<int> cur_node;
                        cur_node.push_back(itr->first);
                        sort(temp_new_comm_mem.begin(),temp_new_comm_mem.end());
                        sort(cur_node.begin(),cur_node.end());
                        vector<int> res=Difference_v(temp_new_comm_mem,cur_node);
                        temp_new_community_node_list[c]=res;
                        temp_new_comm_list[itr->first]=-1;

                        //empty temp vectors
                        temp_new_comm_mem.clear();
                        cur_node.clear();
                        res.clear();

                        //add vis in v's community in temp_new community list
                        //temp_comm_list_new[comm_node(G,v,temp_comm_list_new)]=temp_comm_list_new[comm_node(G,v,temp_comm_list_new)]+[vis]


                        int c_v_temp_new=temp_new_comm_list.find(v)->second;    //v's community in temp_new community list

                        vector<int>c_v_temp_new_mem=temp_new_community_node_list[c_v_temp_new];
                        c_v_temp_new_mem.push_back(itr->first);
                        temp_new_community_node_list[c_v_temp_new]=c_v_temp_new_mem;
                        temp_new_comm_list[itr->first]=c_v_temp_new;

                        //empty temp vectors
                        c_v_temp_new_mem.clear();

                        //Test Print temp community

                        //print_community_list_temp(temp_new_community_node_list, temp_new_comm_list);

                        float p_2=permanence(itr->first, temp_new_comm_list, temp_new_community_node_list);

                        visited[itr->first]=1;

                        //cout << "p_1 value: "<< p_1 << "p_2 value: "<< p_2 <<endl;
                        if(p_2>p_1)
                        {
                            //temp_comm_list[c]=list(set(temp_comm_list[c])-set([vis]))
                            temp_new_comm_mem=temp_community_node_list[c];
                            cur_node.push_back(itr->first);
                            sort(temp_new_comm_mem.begin(),temp_new_comm_mem.end());
                            sort(cur_node.begin(),cur_node.end());
                            res=Difference_v(temp_new_comm_mem,cur_node);

                            temp_community_node_list[c]=res;
                            temp_comm_list[itr->first]=-1;

                            //empty temp vectors
                            temp_new_comm_mem.clear();
                            cur_node.clear();
                            res.clear();

                            //temp_comm_list[comm_node(G,v,temp_comm_list)]=temp_comm_list[comm_node(G,v,temp_comm_list)]+[vis]

                            c_v_temp_new=temp_comm_list.find(v)->second;    //v's community in temp community list

                            c_v_temp_new_mem=temp_community_node_list[c_v_temp_new];
                            c_v_temp_new_mem.push_back(itr->first);
                            temp_community_node_list[c_v_temp_new]=c_v_temp_new_mem;
                            temp_comm_list[itr->first]=c_v_temp_new;


                            //empty temp vectors
                            c_v_temp_new_mem.clear();


                            //TeST PRINT Working
                            // cout << "Print temp community list"  << endl;
                            // print_community_list_temp(temp_community_node_list, temp_comm_list);



                        }
                        else
                        {
                            evaluated.push_back(itr->first);
                        }

                    }
                }
            }


            //queue<int> myqueue_add, myqueue_copy;
            vector<int> myqueue_add;
            //print('Queue after moving to/ from community: ',queue,'\n')

            // cout << "Queue after moving to/ from community: "<< endl;
            // printQueue(myqueue);
            // myqueue_copy=myqueue;

            // cout << "Queue Copy: " << endl;
            // printQueue(myqueue_copy);

            //while (!myqueue_copy.empty())
            for(int a=0;a<myqueue.size();a++)
            {
                // int q=myqueue_copy.front();
                // myqueue_copy.pop();

                int q=myqueue[a];
                if(!node_in_list(q,evaluated))
                {
                    vector<int> neigh, removal;
                    neigh=node_list.find(q)->second;
                    // cout << "Print Initial neigh Vector: " << endl;
                    // printVector(neigh);
                    #pragma omp parallel for shared(neigh,temp_new_comm_list,visited,v,removal)
                    for(int i=0;i<neigh.size();i++)
                    {
                        int n=neigh[i];
                        int n_c=temp_new_comm_list.find(n)->second;
                        map<int,int>::const_iterator got = visited.find (n);
                        int vis_n;
                        if (got == visited.end() )
                        {
                            vis_n=-1;
                        }
                        else
                        {
                            vis_n=got->second;
                        }
                        //if (comm_node(G,n,temp_comm_list_new)!=c) or (n in visited) or (n==v):
                        if((n_c!=c) || (n==v) ||(vis_n==1) )      //|| (visited.find(n)->second==1)
                        {
                            #pragma omp critical
                            {
                                removal.push_back(n);
                            }


                            
                        }
                    }
                    //neigh=(set(neigh)-set(removal))
                    sort(neigh.begin(),neigh.end());
                    sort(removal.begin(),removal.end());
                    neigh=Difference_v(neigh,removal);

                    // cout << "Print neigh Vector: " << endl;
                    // printVector(neigh);
                    // cout << "Print removal Vector: " << endl;
                    // //printVector(removal);
                    // //cout << "Print result=neigh-removal Vector: " << endl;
                    // //printVector(neigh);

                    //visited.update({key: 0 for key in neigh})

                    //que_add+=neigh
                    // #pragma omp parallel for shared (neigh,visited,myqueue_add)
                    for(int i=0;i<neigh.size();i++)
                    {
                        visited[neigh[i]]=0;
                        //myqueue_add.push(neigh[i]);
                        // #pragma omp critical
                        {
                            myqueue_add.push_back(neigh[i]);
                        } 
                    }


                }
                //if(itr->first==comm)result.push_back(itr->first);
            }
            //queue=que_add
            myqueue=myqueue_add;

            //cout << "Updated Queue: " << endl;
            //printQueue(myqueue);
            //printVector(myqueue);
        }
        //diff_c1=c1_perm_old-perm_comm(G,c1,temp_comm_list)
        float diff_c1=c_u_perm_old-permanence_comm(c_u,temp_comm_list,temp_community_node_list);


        //V & ITS NEIGHBORS    MOVEs

        //cout << "V & ITS NEIGHBORS    MOVEs" << endl;

        //map<int,int> visited;
        visited.clear();
        c_u=comm_list.find(u)->second;

        c_v=comm_list.find(v)->second;

        c_u_perm_old=permanence_comm(c_u);
        c_v_perm_old=permanence_comm(c_v);

        //queue<int> myqueue;
        // while (!myqueue.empty())
        // {
        //     myqueue.pop();
        // }

        myqueue.clear();


        //temp community list for v
        map< int,vector<int> > temp_community_node_list2=community_node_list;
        map< int,int > temp_comm_list2=comm_list;

        //myqueue.push(v);
        myqueue.push_back(v);

        visited[v]=0;

        //while (!myqueue.empty())
        while (myqueue.size()>0)
        {
            int c=temp_comm_list.find(myqueue.front())->second;
            vector<int> evaluated;


            map<int,int>:: iterator itr;
            //for vis in visited.keys():
            // #pragma omp parallel for 
            for (itr = visited.begin(); itr!=visited.end(); itr++)
            {
                //if visited[vis]==0 and vis!=u:
                // #pragma omp task
                {
                    if(itr->second==0 && itr->first!=u)
                    {
                        float p_1=permanence(itr->first, temp_comm_list2, temp_community_node_list2);

                        //temp new community list (temp_comm_list_new=temp_comm_list[:])
                        map< int,vector<int> > temp_new_community_node_list=temp_community_node_list2;
                        map< int,int > temp_new_comm_list=temp_comm_list2;


                        //temp_comm_list_new[c]=list(set(temp_comm_list_new[c])-set([vis]))
                        vector<int> temp_new_comm_mem=temp_new_community_node_list[c];
                        vector<int> cur_node;
                        cur_node.push_back(itr->first);
                        sort(temp_new_comm_mem.begin(),temp_new_comm_mem.end());
                        sort(cur_node.begin(),cur_node.end());
                        vector<int> res=Difference_v(temp_new_comm_mem,cur_node);
                        temp_new_community_node_list[c]=res;
                        temp_new_comm_list[itr->first]=-1;

                        //empty temp vectors
                        temp_new_comm_mem.clear();
                        cur_node.clear();
                        res.clear();

                        //add vis in v's community in temp_new community list
                        //temp_comm_list_new[comm_node(G,v,temp_comm_list_new)]=temp_comm_list_new[comm_node(G,v,temp_comm_list_new)]+[vis]


                        int c_v_temp_new=temp_new_comm_list.find(u)->second;    //u's community in temp_new community list

                        vector<int>c_v_temp_new_mem=temp_new_community_node_list[c_v_temp_new];
                        c_v_temp_new_mem.push_back(itr->first);
                        temp_new_community_node_list[c_v_temp_new]=c_v_temp_new_mem;
                        temp_new_comm_list[itr->first]=c_v_temp_new;

                        //empty temp vectors
                        c_v_temp_new_mem.clear();


                        //Test Print temp community

                        //print_community_list_temp(temp_new_community_node_list, temp_new_comm_list);

                        float p_2=permanence(itr->first, temp_new_comm_list, temp_new_community_node_list);

                        visited[itr->first]=1;

                        //cout << "p_1 value: "<< p_1 << "p_2 value: "<< p_2 <<endl;

                        if(p_2>p_1)
                        {
                            //temp_comm_list[c]=list(set(temp_comm_list[c])-set([vis]))
                            temp_new_comm_mem=temp_community_node_list2[c];
                            cur_node.push_back(itr->first);
                            sort(temp_new_comm_mem.begin(),temp_new_comm_mem.end());
                            sort(cur_node.begin(),cur_node.end());
                            res=Difference_v(temp_new_comm_mem,cur_node);

                            temp_community_node_list2[c]=res;
                            temp_comm_list2[itr->first]=-1;

                            //empty temp vectors
                            temp_new_comm_mem.clear();
                            cur_node.clear();
                            res.clear();

                            //temp_comm_list[comm_node(G,v,temp_comm_list)]=temp_comm_list[comm_node(G,v,temp_comm_list)]+[vis]

                            c_v_temp_new=temp_comm_list2.find(u)->second;    //u's community in temp community list

                            c_v_temp_new_mem=temp_community_node_list2[c_v_temp_new];
                            c_v_temp_new_mem.push_back(itr->first);
                            temp_community_node_list2[c_v_temp_new]=c_v_temp_new_mem;
                            temp_comm_list2[itr->first]=c_v_temp_new;


                            //empty temp vectors
                            c_v_temp_new_mem.clear();


                        }
                        else
                        {
                            evaluated.push_back(itr->first);
                        }

                    }
                }
            }


            // queue<int> myqueue_add;

            // while (!myqueue.empty())
            vector<int> myqueue_add;

            for(int a=0;a<myqueue.size();a++)
            {
                // int q=myqueue.front();
                // myqueue.pop();
                int q=myqueue[a];
                if(!node_in_list(q,evaluated))
                {
                    vector<int> neigh, removal;
                    neigh=node_list.find(q)->second;
                    #pragma omp parallel for shared(neigh,temp_comm_list2,visited,u,removal)
                    for(int i=0;i<neigh.size();i++)
                    {
                        int n=neigh[i];
                        int n_c=temp_comm_list2.find(n)->second;
                        map<int,int>::const_iterator got = visited.find (n);
                        int vis_n;
                        if (got == visited.end() )
                        {
                            vis_n=-1;
                        }
                        else
                        {
                            vis_n=got->second;
                        }


                        //if (comm_node(G,n,temp_comm_list_new)!=c) or (n in visited) or (n==v):
                        if((n_c!=c) || (n==u) || (vis_n==1) ) //|| (visited.find(n)->second==1)
                        {
                            #pragma omp critical
                            {
                                removal.push_back(n);
                            }
                            
                        }
                    }
                    //neigh=(set(neigh)-set(removal))
                    sort(neigh.begin(),neigh.end());
                    sort(removal.begin(),removal.end());
                    neigh=Difference_v(neigh,removal);

                    //visited.update({key: 0 for key in neigh})

                    //que_add+=
                    // #pragma omp parallel for shared (neigh,visited,myqueue_add)
                    for(int i=0;i<neigh.size();i++)
                    {
                        visited[neigh[i]]=0;
                        //myqueue_add.push(neigh[i]);
                        // #pragma omp critical
                        {
                            myqueue_add.push_back(neigh[i]);
                        }                            
                        
                    }


                }
                //if(itr->first==comm)result.push_back(itr->first);
            }
            //queue=que_add
            myqueue=myqueue_add;
        }
        //diff_c1=c1_perm_old-perm_comm(G,c1,temp_comm_list)
        float diff_c2=c_v_perm_old-permanence_comm(c_v,temp_comm_list2,temp_community_node_list2);

        /*#retain community structure having greater difference
        if diff_c1>diff_c2:
            comm_list=temp_comm_list
        else:
            comm_list=temp_comm_list2
        */
        if(diff_c1>diff_c2)
        {
            comm_list=temp_comm_list;
            community_node_list=temp_community_node_list;

        }
        else
        {
            comm_list=temp_comm_list2;
            community_node_list=temp_community_node_list2;
        }


    }
    else
    {
        add_4++;
        add_edge(u,v);
    }

    //Test Priiiiiint   => Not working in upper statements

    //print_community_list();


}

void del_cond_1(int u, int v)
{
    int c_u=comm_list.find(u)->second;

    int c_v=comm_list.find(v)->second;

    delete_edge(u,v);
    vector<int>comm_members_node, temp_comm_members;

    //mmmmmmmodify u's community

    comm_members_node=community_node_list.find(c_u)->second;

    if(comm_members_node.size()==1)
    {
        community_node_list.erase(c_u);
    }
    else
    {
        #pragma omp parallel for shared(comm_members_node,temp_comm_members,u)
        for(int i=0;i<comm_members_node.size();i++)
        {
            if(comm_members_node[i]!=u)
            {
                #pragma omp critical
                {
                    temp_comm_members.push_back(comm_members_node[i]);






                }
                
            }
        }
        community_node_list[c_u]=temp_comm_members;
    }

    ////modify v's community

    comm_members_node.empty();
    temp_comm_members.empty();

    comm_members_node=community_node_list.find(c_v)->second;

    if(comm_members_node.size()==1)
    {
        community_node_list.erase(c_v);
    }
    else
    {
        #pragma omp parallel for shared(comm_members_node,temp_comm_members,v) 
        for(int i=0;i<comm_members_node.size();i++)
        {
            if(comm_members_node[i]!=v)
            {
                #pragma omp critical
                {
                    temp_comm_members.push_back(comm_members_node[i]);






                }
            }
        }
        community_node_list[c_v]=temp_comm_members;
    }
    comm_members_node.empty();
    temp_comm_members.empty();

    add_comm(u);
    add_comm(v);
}

void del_cond_2(int u, int v)
{
    int c_v=comm_list.find(v)->second;

    delete_edge(u,v);
    vector<int>comm_members_node, temp_comm_members;
    //remmmmmmove v from its communityyyyy and append as singleton community
    comm_members_node=community_node_list.find(c_v)->second;

    if(comm_members_node.size()==1)
    {
        community_node_list.erase(c_v);
    }
    else
    {
        #pragma omp parallel for shared(comm_members_node,temp_comm_members,v)
        for(int i=0;i<comm_members_node.size();i++)
        {
            if(comm_members_node[i]!=v)
            {
                #pragma omp critical
                {
                    temp_comm_members.push_back(comm_members_node[i]);
                }
            }
        }
        community_node_list[c_v]=temp_comm_members;
    }
    comm_members_node.empty();
    temp_comm_members.empty();


    add_comm(v);

}

void del_cond_3(int u, int v)
{
    int c_u=comm_list.find(u)->second;

    delete_edge(u,v);
    vector<int>comm_members_node, temp_comm_members;
    //remmmmmmove v from its communityyyyy and append as singleton community
    comm_members_node=community_node_list.find(c_u)->second;

    if(comm_members_node.size()==1)
    {
        community_node_list.erase(c_u);
    }
    else
    {
        #pragma omp parallel for shared(comm_members_node,temp_comm_members,u)
        for(int i=0;i<comm_members_node.size();i++)
        {
            if(comm_members_node[i]!=u)
            {
                #pragma omp critical
                {
                    temp_comm_members.push_back(comm_members_node[i]);
                }
            }
        }
        community_node_list[c_u]=temp_comm_members;
    }
    comm_members_node.empty();
    temp_comm_members.empty();


    add_comm(u);

}

void del_cond_4(int u, int v)
{
    /*
    visited={}
        c1=comm_node(G,u,comm_list)
        #c2=comm_node(G,v,comm_list)
        c1_perm_old=perm_comm(G,c1,comm_list)
        queue=[]
        temp_comm_list=comm_list[:]
        G.remove_edge(u,v)
        queue.append(u)
        visited[queue[0]]=0
        while len(queue)>0:
            c=comm_node(G,queue[0],temp_comm_list)
            evaluated=[]
            for vis in visited.keys():
                if visited[vis]==0 and vis!=v:
                    temp_comm_list=[]
                    temp_comm_list.append(vis)
                    visited[vis]=1
                    evaluated.append(vis)
            que_add=[]
            for q in queue:
                if q not in evaluated:
                    neigh=list(G.neighbors(q))
                    removal=[]
                    for n in neigh:
                        if (comm_node(G,n,comm_list)!=c) or (n in visited) or (n==v):
                            removal.append(n)
                    neigh=(set(neigh)-set(removal))
                    visited.update({key: 0 for key in neigh})
                    que_add+=neigh
            queue=que_add




    */

    /*
    if perm_comm(G,0,temp_comm_list2)+perm_comm(G,0,temp_comm_list)> perm_comm(G,c,comm_list):
            comm_list.pop(c)
            comm_list.append(temp_comm_list[0])
            comm_list.append(temp_comm_list2[0])v

    */
    int c=comm_list.find(u)->second;
    float perm_old=permanence_comm(c);
    delete_edge(u,v);
    //temp_comm__list assigning u
    map< int,vector<int> > temp_community_node_list;
    map< int,int > temp_comm_list;
    temp_comm_list[u]=0;
    temp_community_node_list[0]={u};
    //temp_comm__list assigning v
    map< int,vector<int> > temp_community_node_list2;
    map< int,int > temp_comm_list2;
    temp_comm_list2[v]=0;
    temp_community_node_list2[0]={v};

    float perm_u=permanence_comm(0,temp_comm_list,temp_community_node_list);
    float perm_v=permanence_comm(0,temp_comm_list2,temp_community_node_list2);
    //cout << "perm_u=" << perm_u << " perm_v=" << perm_v    << " perm_old=" << perm_old << endl;
    if((perm_u+perm_v)>perm_old)
    {
        vector<int> comm_members=community_node_list.find(c)->second;

        if(comm_members.size()==2)community_node_list.erase(c);
        else
        {
            vector<int> temp;
            #pragma omp parallel for shared(comm_members,temp,u,v)
            for(int i=0;i<comm_members.size();i++)
            {
                if(comm_members[i]!=u || comm_members[i]!=v)
                {
                    #pragma omp critical
                    {
                        temp.push_back(comm_members[i]);
                    }
                }
            }
            community_node_list[c]=temp;
        }


        add_comm(u);
        add_comm(v);
    }





    adjust_comm_list();









    //if perm_comm(G,0,temp_comm_list2)+perm_comm(G,0,temp_comm_list)> perm_comm(G,c,comm_list):

}


void edge_deletion(int u, int v)
{
    //cout << "Deleting Edge: " << u <<"& "<< v<<endl;
    int c_u=comm_list.find(u)->second;

    int c_v=comm_list.find(v)->second;

    //Condition-1
    //if G.has_edge(u,v) and G.degree(u)==1 and G.degree(v)==1:
    if(has_edge(u,v) && degree(u)==1 && degree(v)==1)
    {
        //cout << "Delete Condddddddition 1" <<endl;
        del_1++;
        del_cond_1(u,v);

    }
        //Condition-2
        //elif G.has_edge(u,v) and comm_node(G,u,comm_list)==comm_node(G,v,comm_list) and G.degree(v)==1
    else if(has_edge(u,v) && (c_u==c_v)  &&  degree(v)==1)
    {
        //cout << "Delete Condddddddition 2" <<endl;
        del_2++;
        del_cond_2(u,v);

    }
        //Condition-3
        ////elif G.has_edge(u,v) and comm_node(G,u,comm_list)==comm_node(G,v,comm_list) and G.degree(u)==1
    else if(has_edge(u,v) && (c_u==c_v)  &&  degree(u)==1)
    {
        //cout << "Delete Condddddddition 3" <<endl;
        del_3++;;;
        del_cond_3(u,v);

    }
        //condition-4
        //elif G.has_edge(u,v) and comm_node(G, u, comm_list)==comm_node(G,v,comm_list):
    else if(has_edge(u,v) && (c_u==c_v)  )
    {
        //cout << "Delete Condddddddition 4" <<endl;
        del_4++;
        del_cond_4(u,v);

    }

        //cooondition 5
        ////elif G.has_edge(u,v):
    else if(has_edge(u,v))
    {
        //cout << "Delete Condddddddition 5" <<endl;
        del_5++;
        delete_edge(u,v);
    }

}



void usage(char *prog_name, const char *more)
{
    cerr << more;
    cerr << "usage: " << prog_name << " [-s num_snapshot] [-b start] [-i input_file] [-f community format] [-o output_file]  [-h]" << endl << endl;


    cerr << "-i input_file\tInput File (Edge-List)" << endl;
    cerr << "-f comm_format\t0 for node-community 1 for community list Format" << endl;
    cerr << "-o output_file\tOutput File (Community List)" << endl;

    cerr << "-s num_snapshot\tNumber of Snapshots" << endl;
    cerr << "-b start\tBeginning Snapshot" << endl;
    cerr << "-h\tshow this usage message" << endl;

    exit(0);
}

void parse_args(int argc, char **argv)
{
    if (argc<2)
        usage(argv[0], "Bad arguments number\n");

    for (int i = 1; i < argc; i++)
    {
        if(argv[i][0] == '-')
        {
            switch(argv[i][1]) {
                case 'i' :
                    input_file = argv[i+1];
                    i++;
                    break;

                case 's':
                    num_snap = atoi(argv[i+1]);
                    i++;
                    break;
                case 'b':
                    start = atoi(argv[i+1]);
                    i++;
                    break;
                case 'f':
                    comm_format = atoi(argv[i+1]);
                    i++;
                    break;
                case 'o':
                    output_file = argv[i+1];
                    i++;
                    break;

                case 'h':
                    usage(argv[0], "");
                    break;
                default:
                    usage(argv[0], "Unknown option\n");
            }
        }
        /*else
        {
          if (filename==NULL)
            filename = argv[i];
          else
            usage(argv[0], "More than one filename\n");
        }*/
    }
    /*if (filename == NULL)
      usage(argv[0], "No input file has been provided\n");*/
}

void read_edge_list_initial(char *filename)     //tttttttttttested workiing
{
    ifstream finput;
    finput.open(filename,fstream::in);
    if (finput.is_open() != true) {
        cerr << "The file " << filename << " does not exist" << endl;
        exit(EXIT_FAILURE);
    }

    //unsigned long long nb_links = 0ULL;

    while (!finput.eof())
    {
        int src, dest;
        finput >> src >> dest;

        /*long double weight = 1.0L;

        if (type==WEIGHTED) {
          finput >> src >> dest >> weight;
        } else {
          finput >> src >> dest;
        }
        */
        if (finput&& (src!=dest))
        {
            //Insert  Srcc
            if(node_list.count(src)==0)
            {
                node_list[src]={dest};
            }
            else
            {
                int found=0;
                vector<int> temp=node_list.find(src)->second;


                for(int i=0;i<temp.size();i++)
                {
                    if(temp[i]==dest)
                    {
                        found=1;
                        break;
                    }

                }
                if(found==0)
                {
                    temp.push_back(dest);
                    node_list[src]=temp;
                }
            }

            //Insert Dest

            if(node_list.count(dest)==0)
            {
                node_list[dest]={src};
            }
            else
            {
                int found=0;
                vector<int> temp=node_list.find(dest)->second;
                for(int i=0;i<temp.size();i++)
                {
                    if(temp[i]==src)
                    {
                        found=1;
                        break;
                    }

                }
                if(found==0)
                {
                    temp.push_back(src);
                    node_list[dest]=temp;
                }

            }

            /*if (links.size()<=max(src,dest)+1) {
              links.resize(max(src,dest)+1);
            }

            links[src].push_back(make_pair(dest,weight));
            if (src!=dest)
              links[dest].push_back(make_pair(src,weight));

            nb_links += 1ULL;*/
        }
    }

    finput.close();

    //Test Prinnnnnnnt Workiiiiiiiiiing
    /*   map<int,vector<int>>:: iterator itr;
       for (itr = node_list.begin(); itr != node_list.end(); itr++)
       {
           cout  << itr->first << endl;
           vector<int>temp=itr->second;
           for(int i=0;i<temp.size();i++)
           {
               cout << temp[i] << " ";
           }
           cout << endl;
           //if(itr->first==comm)result.push_back(itr->first);
       }*/

}


vector<pair<int,int>> read_edge_list(char *filename)        //Testtttted Working
{
    vector<pair<int,int>> result;
    ifstream finput;
    finput.open(filename,fstream::in);
    if (finput.is_open() != true) {
        cerr << "The file " << filename << " does not exist" << endl;
        exit(EXIT_FAILURE);
    }

    while (!finput.eof())
    {
        int src, dest;
        finput >> src >> dest;
        if(finput && (src!=dest))
        {
            int found=0;
            pair <int,int> temp;
            temp=make_pair(src,dest);
            if(result.size()==0)result.push_back(temp);
            else
            {
                for(int i=0;i<result.size();i++)
                {
                    if(result[i]==temp)
                    {
                        found=1;
                        break;
                    }
                }
                if(found==0)result.push_back(temp);
            }

        }
    }
    //Test Print
    /*for(int i=0;i<result.size();i++)
    {
      cout << result[i].first << "&& " << result[i].second << endl;
    }*/
    return result;

}


void read_community_initial(char *filename)     ///////////////tested workkkkkkking
{
    ifstream file (filename);
    if (!file) {
        cerr << "unable to open file";
        exit(EXIT_FAILURE);
    }
    string line;
    int i=0;
    while (getline (file, line))
    {
        vector<int>temp;
        //cout << line << endl;
        temp=int_list_from_string(line);
        community_node_list[i]=temp;
        // #pragma omp parallel for schedule(guided)
        for(int j=0;j<temp.size();j++)
        {
            comm_list[temp[j]]=i;
        }

        i++;

    }
    //adjust_comm_list();

    //Test PPPPrint         //Working
    /*
    map<int,vector<int>>:: iterator itr;
    cout << "community_node_list" << endl;
        for (itr = community_node_list.begin(); itr != community_node_list.end(); itr++)
        {
            cout  << itr->first << endl;
            vector<int>temp=itr->second;
            for(int i=0;i<temp.size();i++)
            {
                cout << temp[i] << " ";
            }
            cout << endl;
        }
        cout << "comm_list" << endl;
        map<int,int>:: iterator itr2;
        for (itr2 = comm_list.begin(); itr2 != comm_list.end(); itr2++)
        {
            cout  << itr2->first << "&& " << itr2->second << endl;

        }
        */
}


void read_community_initial_node_comm_format(char *filename)     ///////////////tested workkkkkkking
{
    ifstream finput;
    finput.open(filename,fstream::in);
    if (finput.is_open() != true) {
        cerr << "The file " << filename << " does not exist" << endl;
        exit(EXIT_FAILURE);
    }

    //unsigned long long nb_links = 0ULL;

    while (!finput.eof()) {
        int node, comm;
        finput >> node >> comm;
        comm_list[node]=comm;
    }

    adjust_community_node_list();




    //Test PPPPrint         //Working
/*
    map<int,vector<int>>:: iterator itr;
    cout << "community_node_list" << endl;
        for (itr = community_node_list.begin(); itr != community_node_list.end(); itr++)
        {
            cout  << itr->first << endl;
            vector<int>temp=itr->second;
            for(int i=0;i<temp.size();i++)
            {
                cout << temp[i] << " ";
            }
            cout << endl;
        }
        cout << "comm_list" << endl;
        map<int,int>:: iterator itr2;
        for (itr2 = comm_list.begin(); itr2 != comm_list.end(); itr2++)
        {
            cout  << itr2->first << "&& " << itr2->second << endl;

        }*/

}


void write_community_list(char *out)
{
    ofstream outputfile(out);
    map<int,vector<int>>:: iterator itr;
    //cout << "community_node_list" << endl;
    for (itr = community_node_list.begin(); itr != community_node_list.end(); itr++)
    {
        //cout  << itr->first << " : "<< endl;
        vector<int>temp=itr->second;
        for(int i=0;i<temp.size();i++)
        {
            outputfile << temp[i] << " ";
        }
        outputfile << endl;
    }
}




void input_processing()
{
    long long time_begin_total, time_end_total, time_begin_add, time_end_add, time_begin_delete, time_end_delete;
    long long total_add=0, total_delete=0;
    /*    s += "-" + rankS.str() + ".bin";

    //s += "-" + "1" + ".bin";

    char* tmp = new char[s.length() + 1];
    strcpy(tmp, s.c_str());
***********/


    stringstream start_S;
    start_S  << start;
    string s(input_file);

    if (start<10)
    {
        s += ".t0" + start_S.str();
    }
    else
    {
        s += ".t" + start_S.str();
    }
    //s_old += ".t0"+i_S_old.str()+".edges";
    //s += ".t01.edges";

    string s_c;
    s_c = s+".comm";

    s += ".edges";

//    string s_c(input_file);
//    s_c += ".t01.comm"


    //cout << s << "\t"<<    s_c << endl;
    char *input_file_initial=new char[s.length() + 1];
    strcpy(input_file_initial, s.c_str());

    char *comm_file_initial=new char[s_c.length() + 1];
    strcpy(comm_file_initial, s_c.c_str());


    //read initial edge-list, form graph frrrrrrrrrom snapshot 0
    read_edge_list_initial(input_file_initial);
    // read initial commuuuunitttttty list
    if(comm_format==1)read_community_initial(comm_file_initial);
    else
    {
        read_community_initial_node_comm_format(comm_file_initial);
        //cout << "Distinct Community: " << count_distinct_comm_list() << endl;
//        string s_test(output_file);
//        s_test += ".test.t01.comm";
//
//        char *output_test=new char[s_test.length() + 1];
//        strcpy(output_test, s_test.c_str());
//
//        write_community_list(output_test);
//        read_community_initial(output_test);
//        print_community_list();


    }



    time_begin_total=current_timestamp();
    ////for (int i=2;i<num_snap+1;i++)
    for (int i=start+1;i<num_snap+1;i++)
    {
        char *input_file_old;
        char *input_file_new;

        char *output_file_dump;

        stringstream i_S_old, i_S_new;
        i_S_old << (i-1);
        i_S_new << i;

        //cout << i_S_old.str() << "__" << i_S_new.str() << endl;

        if (i<10)
        {
            string s_old(input_file);
            s_old += ".t0"+i_S_old.str()+".edges";

            input_file_old=new char[s_old.length() + 1];
            strcpy(input_file_old, s_old.c_str());


            string s_new(input_file);
            s_new += ".t0"+i_S_new.str()+".edges";

            input_file_new=new char[s_new.length() + 1];
            strcpy(input_file_new, s_new.c_str());

            string s_out(output_file);
            s_out += ".t0"+i_S_new.str()+".comm";

            output_file_dump=new char[s_out.length() + 1];
            strcpy(output_file_dump, s_out.c_str());

        }

        else if (i==10)
        {
            string s_old(input_file);
            s_old += ".t0"+i_S_old.str()+".edges";

            input_file_old=new char[s_old.length() + 1];
            strcpy(input_file_old, s_old.c_str());


            string s_new(input_file);
            s_new += ".t"+i_S_new.str()+".edges";

            input_file_new=new char[s_new.length() + 1];
            strcpy(input_file_new, s_new.c_str());


            string s_out(output_file);
            s_out += ".t"+i_S_new.str()+".comm";

            output_file_dump=new char[s_out.length() + 1];
            strcpy(output_file_dump, s_out.c_str());

        }

        else
        {
            string s_old(input_file);
            s_old += ".t"+i_S_old.str()+".edges";

            input_file_old=new char[s_old.length() + 1];
            strcpy(input_file_old, s_old.c_str());


            string s_new(input_file);
            s_new += ".t"+i_S_new.str()+".edges";

            input_file_new=new char[s_new.length() + 1];
            strcpy(input_file_new, s_new.c_str());

            string s_out(output_file);
            s_out += ".t"+i_S_new.str()+".comm";

            output_file_dump=new char[s_out.length() + 1];
            strcpy(output_file_dump, s_out.c_str());

        }

        //cout << input_file_old  <<endl;

        vector<pair<int,int>> edges_pair_old, edges_pair_new, edges_added, edges_deleted;

        edges_pair_old=read_edge_list(input_file_old);

        //cout << input_file_new <<endl;

        edges_pair_new=read_edge_list(input_file_new);

        sort(edges_pair_old.begin(),edges_pair_old.end());
        sort(edges_pair_new.begin(),edges_pair_new.end());

        edges_added=Difference_vp(edges_pair_new,edges_pair_old);
        edges_deleted=Difference_vp(edges_pair_old,edges_pair_new);

        //Test PPPPrint
        //cout << "Edges added in current snapshot: "<<i <<  "    is:  "<< edges_added.size() <<endl;
       /*for(int j=0;j<edges_added.size();j++)
       {
           pair <int,int> temp = edges_added[j];
           cout << temp.first << "## " << temp.second<< endl;

       }
   */



       time_begin_add=current_timestamp();
       add_1=0; add_2=0; add_3=0; add_4=0; add_5=0;
        // #pragma omp parallel for shared(edges_added)
       for(int j=0;j<edges_added.size();j++)
       {
           pair <int,int> temp = edges_added[j];
           edge_addition(temp.first, temp.second);
       }
       //cout << "add_cond_1: " << add_1 << " add_cond_2: "<< add_2 << " add_cond_3: "<< add_3 << " add_cond_4: "<< add_4 << endl;
       time_end_add=current_timestamp();

       time_begin_delete=current_timestamp();
//
        //cout << "Edges deleted in current snapshot: "<<i <<  " is:  "<< edges_deleted.size()<<endl;
       /*for(int j=0;j<edges_deleted.size();j++)
       {
           pair <int,int> temp = edges_deleted[j];
           cout << temp.first << "## " << temp.second<< endl;

       }*/
       del_1=0; del_2=0; del_3=0; del_4=0; del_5=0;
       // #pragma omp parallel for schedule(guided)
       for(int j=0;j<edges_deleted.size();j++)
       {
           pair <int,int> temp = edges_deleted[j];
           edge_deletion(temp.first, temp.second);
           //print_community_list();
       }
       //cout << "del_cond_1: " << del_1 << " del_cond_2: "<< del_2 << " del_cond_3: "<< del_3 << " del_cond_4: "<< del_4 << " del_cond_5: "<< del_5 << endl;

       time_end_delete=current_timestamp();

       total_add+=(time_end_add - time_begin_add);
       total_delete+=(time_end_delete - time_begin_delete );

       //cout    << "Snapshot"<< i << " Add Time: " <<(time_end_add - time_begin_add) << " Delete Time: " <<(time_end_delete - time_begin_delete ) <<endl;
       cout    << i << "," <<(time_end_add - time_begin_add) << "," <<(time_end_delete - time_begin_delete ) <<endl;

       //cout    << "Snapshot"<< i << " Final Community" << endl;
       // adjust_comm_list();
       //print_community_list();




       write_community_list(output_file_dump);








    }
//
   time_end_total=current_timestamp();
   /*cout << "Total Duration: " << (time_end_total - time_begin_total) << " milliseconds" << endl;
   cout << "Total Addition Time: " << total_add << " milliseconds" << endl;
   cout << "Total Deletion Time: " << total_delete << " milliseconds" << endl;*/
   cout << total_add << "," << total_delete << "," << (time_end_total - time_begin_total) << endl;
//


}


//************************TEST FUNCTIOOOOOOONS*********************************************
void test_intersection_v()
{
    vector<int> arr1 { 7, 1, 5, 2, 3, 6 };
    vector<int> arr2 { 3, 8, 6, 20, 7 };
    vector<int> arr3;
    //int n1 = sizeof(arr1)/sizeof(arr1[0]);
    //int n2 = sizeof(arr2)/sizeof(arr2[0]);
    int n1 =arr1.size();
    int n2 =arr2.size();
    cout<< "arr1 size: "<<n1<<" arr2 size: "<<n2<<endl;
    //sort(arr1,arr1+n1);
    //sort(arr2,arr2+n2);
    sort(arr1.begin(),arr1.end());
    sort(arr2.begin(),arr2.end());
    for (vector<int>::iterator it = arr1.begin() ; it != arr1.end(); ++it)
    {
        cout << ' ' << *it;
    }
    for (vector<int>::iterator it = arr2.begin() ; it != arr2.end(); ++it)
    {
        cout << ' ' << *it;
    }
    //arr3=Intersection_v(arr1,n1,arr2,n2);
    arr3=Intersection_v(arr2,n2,arr1,n1);
    //cout << "arr3 Size: "<< p << endl;
    for (vector<int>::iterator it = arr3.begin() ; it != arr3.end(); ++it)
    {
        cout << ' ' << *it;
    }
    /*for (int i=0;i<p;i++)
    {
        cout << arr3[i]<< " ";
    }*/

}

void test_difference_v()
{
    vector<int> arr1 { 7, 1, 5, 2, 3, 6 };
    vector<int> arr2 { 3, 8, 6, 20, 7 };
    vector<int> arr3;
    //int n1 = sizeof(arr1)/sizeof(arr1[0]);
    //int n2 = sizeof(arr2)/sizeof(arr2[0]);
    int n1 =arr1.size();
    int n2 =arr2.size();
    cout<< "arr1 size: "<<n1<<" arr2 size: "<<n2<<endl;
    //sort(arr1,arr1+n1);
    //sort(arr2,arr2+n2);
    sort(arr1.begin(),arr1.end());
    sort(arr2.begin(),arr2.end());
    cout << "arr1: "<<endl;
    for (vector<int>::iterator it = arr1.begin() ; it != arr1.end(); ++it)
    {
        cout << ' ' << *it;
    }
    cout << "arr2: "<<endl;
    for (vector<int>::iterator it = arr2.begin() ; it != arr2.end(); ++it)
    {
        cout << ' ' << *it;
    }
    //arr3=Intersection_v(arr1,n1,arr2,n2);
    arr3=Difference_v(arr1,arr2);
    //cout << "arr3 Size: "<< p << endl;
    cout << "arr3: "<<endl;
    for (vector<int>::iterator it = arr3.begin() ; it != arr3.end(); ++it)
    {
        cout << ' ' << *it;
    }
    /*for (int i=0;i<p;i++)
    {
        cout << arr3[i]<< " ";
    }*/

}

void test_find_map_key_max()
{
    map <int, vector<int>> temp;
    temp[5]={2,3,5};

    temp[28881785]={4,2,77950,674,33,22};
    temp[290]={1};
    int p=find_map_key_max(temp);

    cout << p << ": " << endl;
    //temp.find(p)->second << endl;
    //for
}
//*********************************************************************

int main(int argc, char **argv)
{
    //test_intersection_v();
    //test_difference_v();
    //test_find_map_key_max();

    parse_args(argc, argv);

    input_processing();

}
