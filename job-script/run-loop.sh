#!/bin/bash
#t max physical thread
t=8
g++ -std=c++17 -fopenmp loop.cpp -o loop
export  OMP_NUM_THREADS=$t
./loop > 'output.txt'
