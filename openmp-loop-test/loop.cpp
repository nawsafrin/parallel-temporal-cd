#include <omp.h> 
#include <sys/time.h>
#include  <iostream>
#include <vector>
using namespace std;

long long current_timestamp() {
    struct timeval te;
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    // printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}


int main(int argc, char const *argv[])
{
    long long time_begin_stat, time_end_stat, t_stat, time_begin_dyn, time_end_dyn, t_dyn, time_begin_gui, time_end_gui, t_gui=0;
    int nCount[5] = {256, 4096, 65536,  1048576, 16777216};
    for  (int n=0;n<5;n++)
    {
	int p=nCount[n];
	time_begin_stat=current_timestamp();
    	#pragma omp parallel for schedule(static)
	for(int i=0;i<p;i++)
        {
		//do nothing
		
        }
	time_end_stat=current_timestamp();
	t_stat=time_end_stat - time_begin_stat;
	time_begin_dyn=current_timestamp();
	#pragma omp parallel for schedule(dynamic)
	for(int j=0;j<p;j++)
        {
		//do nothing
        }
	time_end_dyn=current_timestamp();
	t_dyn=time_end_dyn - time_begin_dyn;
	time_begin_gui=current_timestamp();
	#pragma omp parallel for schedule(guided)
	for(int k=0;k<p;k++)
        {
		//do nothing
		
        }
	time_end_gui=current_timestamp();
	t_gui=time_end_gui - time_begin_gui;
	cout << nCount[n] << "," << t_stat << "," << t_dyn << "," << t_gui << endl;
    }

return 0;
}
