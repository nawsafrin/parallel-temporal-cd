# temporal-community

Compiling the  code

sequential:
g++ -g -std=c++11 main.cpp -o main

parallel shared memory  (openmp ) code:
g++ -g -std=c++11 -fopenmp main.cpp -o main


Running the code


./main [-s total_number_of_snapshot]  [-i input_file (without extension)] [-f input_community_format (1 for community-list format and 0 for node-community pair format)] [-o output_file_community_list (without extension)]  [-b starting_snapshot(needed only if starting from an intermediate snapshot )] [-h argument_options]

Example Run:
mkdir ../example/s=10/output
./main -i ../example/primary_school/community-list/ps -s 6 -o ../example/primary_school/output/output-list -f 1
./main -i ../example/s=10/community-list/switch -s 10 -o ../example/s=10/output/output-list-Aug13-2021 -f 1


salloc -N 1 -C haswell -q interactive -t 04:00:00
